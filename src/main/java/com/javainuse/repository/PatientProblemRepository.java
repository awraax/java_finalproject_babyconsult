package com.javainuse.repository;

import com.javainuse.model.medical.PatientProblem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientProblemRepository extends JpaRepository<PatientProblem, Integer> {
}
