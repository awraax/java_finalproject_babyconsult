package com.javainuse.repository;

import com.javainuse.model.doctor.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
    @Query(value = "select * from doctor where dokter_id = ?1", nativeQuery = true)
    Doctor findByIdDoctor(int dokter_id);

}
