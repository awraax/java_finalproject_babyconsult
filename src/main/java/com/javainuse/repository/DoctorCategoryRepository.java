package com.javainuse.repository;

import com.javainuse.model.doctor.DoctorCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorCategoryRepository extends JpaRepository<DoctorCategory, Integer> {
}
