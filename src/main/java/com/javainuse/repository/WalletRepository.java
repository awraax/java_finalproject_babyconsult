package com.javainuse.repository;

import com.javainuse.model.transaction.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Integer> {
    @Query(value = "select * from wallet where wallet_id = ?1", nativeQuery = true)
    Wallet findByIdWallet(int wallet_id);
    @Query(value = "select * from wallet where wallet.id_user = ?1", nativeQuery = true)
    Wallet findById_user(int user_id);
//JPGL Apa itu
}
