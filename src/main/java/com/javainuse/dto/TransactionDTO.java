package com.javainuse.dto;

import java.util.Date;

public class TransactionDTO {
    private Date tanggalTransaksi;
    private double totalHarga;

    public Date getTanggalTransaksi() {
        return tanggalTransaksi;
    }
    public void setTanggalTransaksi(Date tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public double getTotalHarga() {
        return totalHarga;
    }
    public void setTotalHarga(double totalHarga) {
        this.totalHarga = totalHarga;
    }
}
