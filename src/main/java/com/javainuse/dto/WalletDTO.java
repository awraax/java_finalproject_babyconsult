package com.javainuse.dto;

public class WalletDTO {

    private int wallet_id;
    private double saldo;

    public WalletDTO(int wallet_id, double saldo){
        this.wallet_id = wallet_id;
        this.saldo = saldo;
    }

    public int getWallet_id() {
        return wallet_id;
    }
    public void setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
    }

    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
