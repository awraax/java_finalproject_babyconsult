package com.javainuse.dto;

import java.util.Date;

public class RecordDTO {

    private Date tanggalPemeriksaan;
    private String diagnosis;
    private String pengobatan;

    public Date getTanggalPemeriksaan() {
        return tanggalPemeriksaan;
    }
    public void setTanggalPemeriksaan(Date tanggalPemeriksaan) {
        this.tanggalPemeriksaan = tanggalPemeriksaan;
    }

    public String getDiagnosis() {
        return diagnosis;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPengobatan() {
        return pengobatan;
    }
    public void setPengobatan(String pengobatan) {
        this.pengobatan = pengobatan;
    }
}
