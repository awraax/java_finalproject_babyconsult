package com.javainuse.dto;

public class PatientProblemDTO {
    private String keluhanPasien;

    public String getKeluhanPasien() {
        return keluhanPasien;
    }
    public void setKeluhanPasien(String keluhanPasien) {
        this.keluhanPasien = keluhanPasien;
    }
}
