package com.javainuse.dto;

public class DoctorDTO {
    private String namaDokter;
    private String jenisKelamin;
    private String asalRumahSakit;
    private String jadwal;

    public String getNamaDokter() {
        return namaDokter;
    }
    public void setNamaDokter(String namaDokter) {
        this.namaDokter = namaDokter;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }
    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAsalRumahSakit() {
        return asalRumahSakit;
    }
    public void setAsalRumahSakit(String asalRumahSakit) {
        this.asalRumahSakit = asalRumahSakit;
    }

    public String getJadwal() {
        return jadwal;
    }
    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }
}
