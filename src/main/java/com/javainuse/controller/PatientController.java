package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.model.medical.PatientProblem;
import com.javainuse.model.medical.Record;
import com.javainuse.model.user.User;
import com.javainuse.repository.*;
import com.javainuse.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class PatientController {
    public static final Logger logger = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private RecordRepository recordRepository;
    @Autowired
    private PatientProblemRepository patientProblemRepository;

    @RequestMapping(value = "/patient-problem/current-user", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createPatientProblem( @RequestBody PatientProblem patientProblem) {
        try {
            User currentUser = userDetailService.findCurrentUser();
            patientProblem.setUsers(currentUser);
            patientProblem.setKeluhanPasien(patientProblem.getKeluhanPasien());

            currentUser.addPatientProblem(patientProblem);
            userRepository.save(currentUser);

            return new ResponseEntity<>(new ResponseHelper("Successfully add patient problem", patientProblem, true), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/medical-record/current-user", method = RequestMethod.GET)
    public ResponseEntity<?> dataMedicalRecordbyUser(){
        try{
            User currentUser = userDetailService.findCurrentUser();
            logger.info("Fetching current medical record SUCCESS!");

            List<PatientProblem> currentProblem = currentUser.getPatientProblem();
            logger.info("Fetching Medical Record Data", currentProblem);

            return new ResponseEntity<>(new ResponseHelper("Retrieve Medical Record SUCCESS!", currentProblem, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve Medical Record FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/medical-record/current-user/{record_id}", method = RequestMethod.GET)
    public ResponseEntity<?> dataMedicalRecordbyId(@PathVariable("record_id") int record_id){
        try{
            logger.info("Fetching Record with id {}", record_id);
            Record record = recordRepository.findById(record_id).get();

            PatientProblem patientProblem = record.getPatientProblem();

            return new ResponseEntity<>(new ResponseHelper("Retrieve User's Medical Record SUCCESS!", patientProblem, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve User's Medical Record FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }
}