package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.model.user.User;
import com.javainuse.repository.UserRepository;
import com.javainuse.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class UserController {
    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailService userDetailService;

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> GetUserInfo() {
        try{
            List<User> user =userRepository.findAll();
            userRepository.findAll();
            logger.info("Fetching all users data SUCCESS!");

            return new ResponseEntity<>( new ResponseHelper("Successfully get all user data", user,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve all users data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/current_user", method = RequestMethod.GET)
    public ResponseEntity<?> findCurrentUser() {
        try {
            User currentUser = userDetailService.findCurrentUser();
            logger.info("Fetching current user data SUCCESS!");

            return new ResponseEntity<>(new ResponseHelper("Successfully get current user", currentUser, true), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve current user data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/count-user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> GetWholeUser() {
        logger.info("Count all user...");
        try{
            long countUser = userRepository.count();
            return new ResponseEntity<>(new ResponseHelper ("Successfully count user", countUser,true),  HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }
}