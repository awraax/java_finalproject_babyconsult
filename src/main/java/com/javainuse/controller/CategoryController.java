package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.model.doctor.DoctorCategory;
import com.javainuse.repository.DoctorCategoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.SQLException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class CategoryController {

    public static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private DoctorCategoryRepository doctorCategoryRepository;

    @RequestMapping(value = "/category-doctor", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createCategory(@RequestBody DoctorCategory doctorCategory) {
        try{
            DoctorCategory newCategory = new DoctorCategory();
            newCategory.setNama_kategori(doctorCategory.getNama_kategori());

            doctorCategoryRepository.save(doctorCategory);

            logger.info("Creating Category Doctor : {}", doctorCategory.getNama_kategori());

            return new ResponseEntity<>(new ResponseHelper<>("Successfully creating category doctor", doctorCategory, true), HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage(),400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/get-category-doctor", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getCategory(){
        logger.info("Get All Category");
        try {
            List<DoctorCategory> doctorCategory = (List<DoctorCategory>) doctorCategoryRepository.findAll();
            doctorCategoryRepository.findAll();

            logger.info("Fetching all Category Doctor data SUCCESS!");

            return new ResponseEntity<>(new ResponseHelper ("Successfully get all category doctor", doctorCategory,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/update-category-doctor/{kategori_id}", method = RequestMethod.POST)
    public ResponseEntity<?> updateCategory(@PathVariable("kategori_id") int kategori_id, @RequestBody DoctorCategory doctorCategory) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & editing category with id: {}", kategori_id);
        try {
            DoctorCategory currentDoctorCategory = doctorCategoryRepository.findById(kategori_id).get();
            currentDoctorCategory.setKategori_id(kategori_id);
            currentDoctorCategory.setNama_kategori(doctorCategory.getNama_kategori());

            doctorCategoryRepository.save(currentDoctorCategory);

            logger.info("Update a Category doctor with ID : {} SUCCESS!", currentDoctorCategory.getKategori_id());

            return new ResponseEntity<>(new ResponseHelper ("Successfully updating category doctor", currentDoctorCategory,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete-category-doctor/{kategory_id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCategory(@PathVariable("kategory_id") int kategory_id) {
        logger.info("Fetching & deleting category with id {}", kategory_id);
        try {
            DoctorCategory currentDoctorCategory = doctorCategoryRepository.findById(kategory_id).get();
            doctorCategoryRepository.deleteById(kategory_id);

            logger.info("Fetching & Deleting a Category Doctor with ID : {} SUCCESS!", kategory_id);
            return new ResponseEntity<>(new ResponseHelper("Fetching & Deleting a Category Doctor SUCCESS!", currentDoctorCategory, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }
}