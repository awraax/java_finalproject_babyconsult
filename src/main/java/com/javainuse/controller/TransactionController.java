package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.model.doctor.Doctor;
import com.javainuse.model.transaction.Transaction;
import com.javainuse.model.transaction.Wallet;
import com.javainuse.model.user.User;
import com.javainuse.repository.DoctorRepository;
import com.javainuse.repository.TransactionRepository;
import com.javainuse.repository.UserRepository;
import com.javainuse.repository.WalletRepository;
import com.javainuse.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class TransactionController {
    public static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    @RequestMapping(value = "/transaction/{doctor_id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createTransaction(@PathVariable("doctor_id") int doctor_id, @RequestBody Map<String, Object> transaction) {
        logger.info("Creating a Transaction : {}", transaction);

        try{
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = userDetails.getUsername();
            User currentUser = userRepository.findByUsername(username);

            Doctor createDoctor = doctorRepository.findById(doctor_id).get();
            logger.info("Fetching doctor data with ID : {}", createDoctor.getDokter_id());

            Transaction createTransaction = new Transaction();
            createTransaction.setUser(currentUser);
            createTransaction.setDoctor(createDoctor);

            createTransaction.setTotalHarga(Double.parseDouble(transaction.get("totalHarga").toString()));
            createTransaction.setStatusPayment(transaction.get("statusPayment").toString());

            transactionRepository.save(createTransaction);

            return new ResponseEntity<>(new ResponseHelper<>("Transaction SUCCESS!", createTransaction, true), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Transaction FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/payment/{transaksi_id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addPayment(@PathVariable("transaksi_id") int transaksi_id)  {
        try{
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = userDetails.getUsername();
            User currentUser = userRepository.findByUsername(username);
            logger.info("Current user found with id : {}", currentUser.getUser_id());

            Wallet newWallet = walletRepository.findById_user(currentUser.getUser_id());
            logger.info("Fetching wallet data with ID : {}", newWallet.getWallet_id());

            Transaction currentTransaction = transactionRepository.findByTransaksiId(transaksi_id);
            logger.info("Get transaction SUCCESS! {}", currentTransaction.getTransaksi_id());

            if (newWallet.getSaldo() >= currentTransaction.getTotalHarga()){
                Doctor createDoctor = doctorRepository.findByIdDoctor(currentTransaction.getDoctor().getDokter_id());
                logger.info("Get doctor SUCCESS!", createDoctor.getDokter_id());

                if (currentTransaction.getStatusPayment().equals("Unpaid")){
                    double updateSaldo = newWallet.getSaldo() - currentTransaction.getTotalHarga();
                    newWallet.setSaldo(updateSaldo);
                    currentTransaction.setStatusPayment("Paid");

                    walletRepository.save(newWallet);
                    logger.info("Current saldo wallet : {}",newWallet.getSaldo());
                } else {
                    logger.info("Payment Failed");
                }

                createDoctor.addTransaction(currentTransaction);
                logger.info("Payment SUCCESS!");

                doctorRepository.save(createDoctor);
                transactionRepository.save(currentTransaction);

                return new ResponseEntity<>(new ResponseHelper<>("Payment SUCCESS!", currentTransaction, true), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ResponseHelper<>("Not Enough Balance" ,null, false), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Payment FAILED!", 400), HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/transaction-history", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> dataHistoryTransaction() {
        logger.info("Fetching all history transactions data SUCCESS!");
        try {
            List<Transaction> transactions = transactionRepository.findAll();

            return new ResponseEntity<>(new ResponseHelper("Retrieve all history transactions data SUCCESS!", transactions, true), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/transaction-history/current-user", method = RequestMethod.GET)
    public ResponseEntity<?> dataHistoryTransactionbyUser(){
        try{
            User currentUser = userDetailService.findCurrentUser();
            logger.info("Fetching current user data SUCCESS!");

            List<Transaction> currentTransaction = currentUser.getTransactions();
            logger.info("Fetching User's History Transaction Data", currentTransaction);

            return new ResponseEntity<>(new ResponseHelper("Retrieve User's History Transaction data SUCCESS!", currentTransaction, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve User's History Transaction FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/total-transaction", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> GetTransactionTotal() {
        logger.info("Count all Transaction...");
        try{
            List<Transaction> transaction;
            transaction = transactionRepository.findByStatusPayment("Paid");
            return new ResponseEntity<>(new ResponseHelper ("Successfully get all transaction", transaction,true),  HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }



}