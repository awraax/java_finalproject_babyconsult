package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.model.doctor.Doctor;
import com.javainuse.model.doctor.DoctorCategory;
import com.javainuse.model.medical.PatientProblem;
import com.javainuse.model.medical.Record;
import com.javainuse.model.user.User;
import com.javainuse.repository.*;
import com.javainuse.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class DoctorController {
    public static final Logger logger = LoggerFactory.getLogger(DoctorController.class);

    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private RecordRepository recordRepository;
    @Autowired
    private PatientProblemRepository patientProblemRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private DoctorCategoryRepository doctorCategoryRepository;
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/doctor/{kategori_id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createDataDoctor(@PathVariable("kategori_id") int kategori_id, @RequestBody Doctor doctor){
        try {
            DoctorCategory doctorCategory = doctorCategoryRepository.findById(kategori_id).get();
            doctor.setNamaDokter(doctor.getNamaDokter());
            doctor.setJenisKelamin(doctor.getJenisKelamin());
            doctor.setAsalRumahSakit(doctor.getAsalRumahSakit());
            doctor.setJadwal(doctor.getJadwal());

            doctor.setDoctorCategory(doctorCategory);
            doctorCategory.addDoctor(doctor);
            doctorRepository.save(doctor);
            doctorCategoryRepository.save(doctorCategory);

            return new ResponseEntity<>(new ResponseHelper("Successfully add doctor", doctor, true), HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/data-doctor", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> GetDataDoctor() {
        try{
            logger.info("Check doctor info...");
            List<Doctor> doctor = doctorRepository.findAll();

            doctorRepository.findAll();

            return new ResponseEntity<>( new ResponseHelper("Successfully get all user data doctor", doctor,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve all doctors data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping (value = "/delete-doctor/{dokter_id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDoctor(@PathVariable("dokter_id") int dokter_id){
        try{
            Doctor currentDoctor = doctorRepository.findById(dokter_id).get();
            doctorRepository.deleteById(dokter_id);

            logger.info("Fetching & Deleting a Doctor with id {}", dokter_id);

            return new ResponseEntity<>(new ResponseHelper("Fetching & Deleting a Doctor SUCCESS!", currentDoctor, true), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage() + "Fetching & Deleting Doctor FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/update-doctor/{dokter_id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDoctor(@PathVariable("dokter_id") int dokter_id, @RequestBody Doctor doctor){
        try{
            Doctor currentDoctor = doctorRepository.findById(dokter_id).get();
            currentDoctor.setNamaDokter(doctor.getNamaDokter());
            currentDoctor.setJenisKelamin(doctor.getJenisKelamin());
            currentDoctor.setAsalRumahSakit(doctor.getAsalRumahSakit());
            currentDoctor.setJadwal(doctor.getJadwal());

            doctorRepository.save(currentDoctor);

            logger.info("Update a Doctor with ID : {} SUCCESS!", dokter_id);

            return new ResponseEntity<>(new ResponseHelper("Update a Doctor SUCCESS!", currentDoctor, true), HttpStatus.OK);
        } catch(Exception e){
            return new ResponseEntity<>(new ResponseErrorHelper(e.getCause().getCause().getMessage() + "Update a Doctor FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/medical-record/doctor/{keluhan_id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createMedicalRecord(@PathVariable("keluhan_id") int keluhan_id, @RequestBody Record record) {
        try{
            PatientProblem patientProblem = patientProblemRepository.findById(keluhan_id).get();
            record.setDiagnosis(record.getDiagnosis());
            record.setPengobatan(record.getPengobatan());

            record.setPatientProblem(patientProblem);

            recordRepository.save(record);
            return new ResponseEntity<>( new ResponseHelper("Successfully create patient problem data", record,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Create patient problem data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/patient-problem/doctor/current-user", method = RequestMethod.GET)
    public ResponseEntity<?> getPatientProblem(){
        try{
            User currentUser = userDetailService.findCurrentUser();
            logger.info("Fetching current patient problem data SUCCESS!");

            logger.info("Fetching PatientProblem with id {}", currentUser);
            List<PatientProblem> currentProblem = currentUser.getPatientProblem();

            recordRepository.findAll();

            return new ResponseEntity<>( new ResponseHelper("Successfully get all patient problem data", currentProblem,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve all patient problem data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/data-doctor/{kategori_id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> GetDataDoctorById(@PathVariable("kategori_id") int kategori_id){
        try {
            DoctorCategory doctorCategory = doctorCategoryRepository.findById(kategori_id).get();
            logger.info("Fetching DoctorCategory with id {}", kategori_id);

            List<Doctor> currentDoctor = doctorCategory.getDoctor();
            return new ResponseEntity<>( new ResponseHelper("Successfully get a doctor data", currentDoctor,true),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Retrieve a doctor data FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/count-doctor", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> GetWholeDoctor() {
        logger.info("Count all doctor...");
        try{
            long countDoctor = doctorRepository.count();
            return new ResponseEntity<>(new ResponseHelper ("Successfully count doctor", countDoctor,true),  HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }
}