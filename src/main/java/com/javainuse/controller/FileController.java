package com.javainuse.controller;

import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/baby-consult")
public class FileController {
    public static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/uploads", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
        try {
            final String fileName = fileService.uploadFile(file);
            return new ResponseEntity<>(new ResponseHelper("Upload file SUCCESS!", fileName, true), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseErrorHelper("Upload file FAILED!", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/files/{filename}:.+", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = fileService.load(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"" ).body(file);
    }
}
