package com.javainuse.controller.auth;


import com.javainuse.common.ResponseErrorHelper;
import com.javainuse.common.ResponseHelper;
import com.javainuse.config.auth.TokenUtil;
import com.javainuse.dto.UserDTO;
import com.javainuse.model.auth.Request;
import com.javainuse.model.auth.Response;
import com.javainuse.model.user.User;
import com.javainuse.repository.UserRepository;
import com.javainuse.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailService userDetailsService;


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody Request authenticationRequest) throws Exception {
        try {
            authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

            final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

            final String token = tokenUtil.generateToken(userDetails);

            User user = userRepository.findByUsername(userDetails.getUsername());
            Response response = new Response(token, user.getRole());

            return new ResponseEntity<>(new ResponseHelper("Login SUCCESS!", response, true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
        try {
            final User users = userDetailsService.save(user);
            return new ResponseEntity<> (new ResponseHelper("Register New User SUCCESS!", users, true), HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<> (new ResponseErrorHelper(e.getCause().getCause().getMessage(), 400), HttpStatus.BAD_REQUEST);
        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
