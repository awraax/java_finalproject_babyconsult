package com.javainuse.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.medical.PatientProblem;
import com.javainuse.model.transaction.Transaction;
import com.javainuse.model.transaction.Wallet;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int user_id;
    @Column(nullable = false, length = 50)
    private String username;
    @Column(nullable = false, length = 50)
    private String email;
    @Column(nullable = false, length = 15)
    private String noTelp;
    @JsonIgnore
    @Column(nullable = false)
    private String password;
    @Column
    private String role;

    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoTelp() {
        return noTelp;
    }
    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    @OneToMany(mappedBy = "users", cascade = CascadeType.ALL, orphanRemoval = true)
    List<PatientProblem> patientProblem = new ArrayList<>();

    public void addPatientProblem(PatientProblem detailPatientProblem){
        patientProblem.add(detailPatientProblem);
        detailPatientProblem.setUsers(this);
    }

    public void removePatientProblem (PatientProblem detailPatientProblem){
        patientProblem.remove(patientProblem);
    }
    public List<PatientProblem> getPatientProblem(){
        return patientProblem;
    }
    public void setPatientProblem(List<PatientProblem> patientProblem){
        this.patientProblem = patientProblem;
    }

    @OneToMany(mappedBy = "users", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Transaction> transactions = new ArrayList<>();

    public void addTransaction(Transaction detailTransaction){
        transactions.add(detailTransaction);
        detailTransaction.setUser(this);
    }

    public void removeTransaction(Transaction detailTransaction){
        transactions.remove(detailTransaction);
    }
    public List<Transaction> getTransactions(){
        return transactions;
    }
    public void setTransactions(List<Transaction> transactions){
        this.transactions = transactions;
    }

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }
    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

}
