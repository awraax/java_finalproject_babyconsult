package com.javainuse.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.doctor.Doctor;
import com.javainuse.model.user.User;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "transaction_patient")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int transaksi_id;
    @Column(nullable = false)
    private LocalDate tanggalTransaksi = LocalDate.now();
    @Column
    private double totalHarga;
    @Column
    private String statusPayment;

    public int getTransaksi_id() {
        return transaksi_id;
    }
    public void setTransaksi_id(int transaksi_id) {
        this.transaksi_id = transaksi_id;
    }

    public LocalDate getTanggalTransaksi() {
        return tanggalTransaksi;
    }
    public void setTanggalTransaksi(LocalDate tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public double getTotalHarga() {
        return totalHarga;
    }
    public void setTotalHarga(double totalHarga) {
        this.totalHarga = totalHarga;
    }

    public String getStatusPayment() {
        return statusPayment;
    }

    public boolean setStatusPayment(String statusPayment) {
        this.statusPayment = statusPayment;
        return false;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "id_doktertrans")
    private Doctor doctor;

    public Doctor getDoctor() {
        return doctor;
    }
    public void setDoctor(Doctor doctor){
        this.doctor = doctor;
    }

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "id_usertrans")
    private User users;
    @JsonIgnore
    public User getUser() {
        return users;
    }
    public void setUser(User users){
        this.users = users;
    }
}
