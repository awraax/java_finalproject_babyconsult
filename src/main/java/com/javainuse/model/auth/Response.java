package com.javainuse.model.auth;

import java.io.Serializable;

public class Response implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private String jwtrole;

    public Response(String jwttoken, String jwtrole) {
        this.jwttoken = jwttoken;
        this.jwtrole = jwtrole;
    }

    public String getJwttoken() {
        return jwttoken;
    }
    public String getJwtrole() {
        return jwtrole;
    }

    public void setJwtrole(String jwtrole) {
        this.jwtrole = jwtrole;
    }
    public String getToken() {
        return this.jwttoken;
    }
}

