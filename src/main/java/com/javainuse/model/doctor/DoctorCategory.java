package com.javainuse.model.doctor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "doctor_category")
public class DoctorCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int kategori_id;
    @Column(nullable = false, length = 50)
    private String nama_kategori;

    public int getKategori_id() {
        return kategori_id;
    }
    public void setKategori_id(int kategori_id) {
        this.kategori_id = kategori_id;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }
    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    @OneToMany(mappedBy = "doctorCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Doctor> doctor = new ArrayList<>();

    public void addDoctor(Doctor detailDoctor){
        doctor.add(detailDoctor);
        detailDoctor.setDoctorCategory(this);
    }

    public void removeDoctor(Doctor detailDoctor){
        doctor.remove(detailDoctor);
    }
    @JsonIgnore
    public List<Doctor> getDoctor(){
        return doctor;
    }
    public void setDoctor(List<Doctor> doctor){
        this.doctor = doctor;
    }
}
