package com.javainuse.model.doctor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.transaction.Transaction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int dokter_id;
    @Column(nullable = false, length = 50)
    private String namaDokter;
    @Column(nullable = false, length = 10)
    private String jenisKelamin;
    @Column(nullable = false, length = 25)
    private String asalRumahSakit;
    @Column(nullable = false, length = 25)
    private String jadwal;

    public int getDokter_id() {
        return dokter_id;
    }
    public void setDokter_id(int dokter_id) {
        this.dokter_id = dokter_id;
    }

    public String getNamaDokter() {
        return namaDokter;
    }
    public void setNamaDokter(String namaDokter) {
        this.namaDokter = namaDokter;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }
    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAsalRumahSakit() {
        return asalRumahSakit;
    }

    public void setAsalRumahSakit(String asalRumahSakit) {
        this.asalRumahSakit = asalRumahSakit;
    }

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    @ManyToOne
    @JoinColumn(name = "id_kategori")
    private DoctorCategory doctorCategory;
    public DoctorCategory getDoctorCategory() {
        return doctorCategory;
    }
    public void setDoctorCategory(DoctorCategory doctorCategory){
        this.doctorCategory = doctorCategory;
    }

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Transaction> transactions = new ArrayList<>();
    public void addTransaction(Transaction detailTransaction){
        transactions.add(detailTransaction);
        detailTransaction.setDoctor(this);
    }

    public void removeTransaction(Transaction detailTransaction){
        transactions.remove(detailTransaction);
    }
    @JsonIgnore
    public List<Transaction> getTransactions(){
        return transactions;
    }
    public void setTransactions(List<Transaction> transactions){
        this.transactions = transactions;
    }

}
