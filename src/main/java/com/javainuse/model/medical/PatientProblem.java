package com.javainuse.model.medical;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javainuse.model.doctor.Doctor;
import com.javainuse.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "patient_problem")
public class PatientProblem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int keluhan_id;
    @Column(nullable = false)
    private String keluhanPasien;

    public int getKeluhan_id() {
        return keluhan_id;
    }
    public void setKeluhan_id(int keluhan_id) {
        this.keluhan_id = keluhan_id;
    }

    public String getKeluhanPasien() {
        return keluhanPasien;
    }
    public void setKeluhanPasien(String keluhanPasien) {
        this.keluhanPasien = keluhanPasien;
    }

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User users;
    @JsonIgnore
    public User getUsers() {
        return users;
    }
    public void setUsers(User user){
        this.users = user;
    }

    @OneToOne(mappedBy = "patientProblem", cascade = CascadeType.ALL,orphanRemoval = true)
    private Record record;

    public Record getRecord() {
        return record;
    }
    public void setRecord(Record record){
        this.record = record;
    }
}
