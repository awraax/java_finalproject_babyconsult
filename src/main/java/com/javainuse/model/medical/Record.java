package com.javainuse.model.medical;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "medical_record")
public class Record {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int record_id;
    @Column(nullable = false)
    private LocalDate tanggalPemeriksaan = LocalDate.now();
    @Column(nullable = false, length = 255)
    private String diagnosis;
    @Column(nullable = false, length = 255)
    private String pengobatan;

    public int getRecord_id() {
        return record_id;
    }
    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public LocalDate getTanggalPemeriksaan() {
        return tanggalPemeriksaan;
    }
    public void setTanggalPemeriksaan(LocalDate tanggalPemeriksaan) {
        this.tanggalPemeriksaan = tanggalPemeriksaan;
    }

    public String getDiagnosis() {
        return diagnosis;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPengobatan() {
        return pengobatan;
    }
    public void setPengobatan(String pengobatan) {
        this.pengobatan = pengobatan;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_keluhan")
    @JsonIgnore
    private PatientProblem patientProblem;

    public PatientProblem getPatientProblem(){
        return patientProblem;
    }
    public void setPatientProblem(PatientProblem patientProblem){
        this.patientProblem = patientProblem;
    }
}
