package com.javainuse.service;
import com.javainuse.dto.UserDTO;
import com.javainuse.model.transaction.Wallet;
import com.javainuse.model.user.User;
import com.javainuse.repository.UserRepository;
import com.javainuse.repository.WalletRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class UserDetailService implements UserDetailsService {
    public static final Logger logger = LoggerFactory.getLogger(UserDetailService.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    //    Kenapa harus di override
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Creating User : {}", username);
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public User save(UserDTO user) {
        User newUser = new User();
        Wallet newWallet = new Wallet();

        newUser.setEmail(user.getEmail());
        newUser.setNoTelp(user.getNoTelp());
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setRole(user.getRole());

        newWallet.setSaldo(500000);
        newWallet.setUser(newUser);
        walletRepository.save(newWallet);

        User userUpdateWallet = userRepository.findById(newUser.getUser_id()).get();
        userUpdateWallet.setWallet(newWallet);
        return userRepository.save(userUpdateWallet);
    }

    public User findCurrentUser(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = userDetails.getUsername();
        return userRepository.findByUsername(username);
    }

}